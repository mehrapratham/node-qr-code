var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var pdf = require("pdf-creator-node");
var path = require("path");
const QRCode = require('qrcode')
const child_process = require('child_process');
var Handlebars = require('handlebars');

Handlebars.unregisterPartial("ifCond", "checkCond");

Handlebars.registerHelper("inc", function(value, options)
{
    return parseInt(value) + 1;
});

Handlebars.registerHelper("ifCond", function(index, operator, limit, options) {
    switch (operator) {
        case 'eq':
            return (index == limit) ? options.fn(this) : options.inverse(this);
        case 'less':
            return (index < limit) ? options.fn(this) : options.inverse(this);
        case 'greater':
            return (index > limit) ? options.fn(this) : options.inverse(this);
        case 'greaterless':
            return (index > limit) ? options.fn(this) : options.inverse(this);
        case 'percent':
            index = index + 1
            return ( (index%limit) == 0) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

const fs = require('fs');
let options = {
    format: "A4",
    "orientation": "portrait",
}


app.use(express.static(path.join(__dirname, 'qrCodesPdfs')));

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(bodyParser.json());


// index page
app.get('/', function(req, res) {
  res.render('pages/index');
});

const generateQR = (text) => {
    return new Promise((resolve) => {
        QRCode.toDataURL(text, {type: 'image/jpeg', margin: 0, quality: 1, color: { light: '#eccecd' }}, function (err, url) {
            if(err) return 
            resolve(url)
        })
    })
}

const generateProductNumber = (number) => {
    let currentNumber = `0000${number}`;
    return currentNumber.substr(currentNumber.length - 4)
}

const generatePDF = (data, index) => {
    let html = fs.readFileSync('./template.html', 'utf8');
    let dirpath = './qrCodesPdfs/'
    let fileName = `qrcode-box-${index+1}.pdf`
    let path = dirpath+""+fileName
    if(!fs.existsSync(dirpath)){
      fs.mkdirSync(dirpath, { recursive: true })
    }

    let document = {
        html: html,
        data: {
            qrCodes: data
        },
        path: path
    };

    return new Promise(resolve => {
      pdf.create(document, options)
        .then(res => {
            resolve(true)
        })
        .catch(error => {
            console.log(error,"error")
            resolve()
        })
    });
}

app.post('/create',(req,res)=>{
    fs.rmdirSync('./qrCodesPdfs', { recursive: true });
    let data = {
        type:"B",
        box:parseInt(req.body.box),
        code:req.body.code,
        quantity:parseInt(req.body.quantity)
    }

    let totalSheets = data.box ? data.box : 1;

    let QRCodes = [...Array(totalSheets)].map(async (item, index) => {
        let currentSheet = {};
        if(data.box) {
            currentSheet['box'] = await generateQR(`B-${index+1}-${data.code}`)
        }
        let products = [];
        [...Array(data.quantity)].map(async (qty, i) => {
            
            products.push(generateQR(`P-${index+1}-${data.code}-${generateProductNumber(i+1)}`));
        })
        await Promise.all(products).then(Products => {
            let allProducts = Products.map(pro =>{
                let product = {
                    url : pro,
                    box : (data.box) ? true : false
                }
                return product
            })
            
            currentSheet['products'] = allProducts;
        })
        return currentSheet
    });
    let array = []
    Promise.all(QRCodes).then(data => {
        data.map((item, indx) => {
            array.push(generatePDF(item, indx));
        })
        Promise.all(array).then(data => {
            child_process.execSync(`zip -r qrCodesPdfs *`, {
                cwd: './qrCodesPdfs'
              });
              res.status(200).send({url: '/qrCodesPdfs.zip'}); 
            
        })
    })
})

app.listen(8080);
console.log('Server is listening on port 8080');